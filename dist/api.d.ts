export interface FetchAPI {
    (url: string, init?: any): Promise<any>;
}
export interface FetchArgs {
    url: string;
    options: any;
}
export declare class BaseAPI {
    basePath: string;
    fetch: FetchAPI;
    constructor(fetch?: FetchAPI, basePath?: string);
}
/**
 * Объект автокомплита для банка
 */
export interface AutocompleteBankData {
    /**
     * БИК банка
     */
    "bankBik"?: string;
    /**
     * К/С банка
     */
    "bankKs"?: string;
    /**
     * Название банка
     */
    "bankName"?: string;
}
/**
 * Автокомплит
 */
export interface AutocompleteBankDataList extends Array<AutocompleteBankData> {
}
/**
 * Объект автокомплита для компании
 */
export interface AutocompleteCompanyData {
    /**
     * Адрес
     */
    "address"?: string;
    /**
     * ИНН
     */
    "inn"?: string;
    /**
     * КПП
     */
    "kpp"?: string;
    /**
     * Название
     */
    "name"?: string;
    /**
     * ОГРН
     */
    "ogrn"?: string;
    /**
     * А/Я
     */
    "postOfficeBox"?: string;
    /**
     * Почтовый индекс
     */
    "postalIndex"?: string;
}
/**
 * Автокомплит
 */
export interface AutocompleteCompanyDataList extends Array<AutocompleteCompanyData> {
}
/**
 * Автокомплит
 */
export interface AutocompleteList extends Array<AutocompleteListItem> {
}
/**
 * Объект автокомплита
 */
export interface AutocompleteListItem {
    /**
     * Идентификатор
     */
    "id"?: string;
    /**
     * Название
     */
    "label"?: string;
}
export interface Body {
    /**
     * Тип аккаунта
     */
    "accountType"?: BodyAccountTypeEnum;
    "address"?: DashboardregistrationsubmitAddress;
    "bankDetails"?: DashboardregistrationsubmitBankDetails;
    /**
     * Имя
     */
    "firstName"?: string;
    /**
     * ИНН
     */
    "inn"?: number;
    /**
     * По городу
     */
    "isByTown"?: number;
    /**
     * НДС
     */
    "isWithVat"?: number;
    /**
     * КПП
     */
    "kpp"?: number;
    /**
     * Фамилия
     */
    "lastName"?: string;
    "legalAddress"?: DashboardregistrationsubmitLegalAddress;
    /**
     * Отчество
     */
    "middleName"?: string;
    /**
     * Название компании
     */
    "name"?: string;
    /**
     * ОГРН/ОГРНИП
     */
    "ogrn"?: number;
    "passport"?: DashboardregistrationsubmitPassport;
    /**
     * Город
     */
    "town"?: string;
    /**
     * Тип
     */
    "type"?: BodyTypeEnum;
    "vehicle"?: DashboardregistrationsubmitVehicle;
}
export declare type BodyAccountTypeEnum = "contractor" | "customer";
export declare type BodyTypeEnum = "businessman" | "legal_person" | "physical_person";
export interface Body1 {
    "person"?: PersonRegistrationForm;
    "user"?: UserRegistrationForm;
    "vehicle"?: VehicleRegistrationForm;
}
/**
 * Рассчитанная цена
 */
export interface CalculatedRecommendedPrice {
    /**
     * Цена за доп. час
     */
    "extraHourPrice"?: number;
    /**
     * Цена
     */
    "price"?: number;
    /**
     * Был расчет или нет
     */
    "result"?: boolean;
}
/**
 * Карта
 */
export interface Card {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * CVV
     */
    "cvv"?: number;
    /**
     * Месяц окончания
     */
    "expireMonth"?: number;
    /**
     * Год окончания
     */
    "expireYear"?: number;
    /**
     * Номер
     */
    "number"?: number;
    /**
     * Владелец
     */
    "owner"?: string;
    /**
     * Статус
     */
    "status"?: CardStatusEnum;
}
export declare type CardStatusEnum = "" | "binded" | "binded_temp" | "error" | "unbinded";
/**
 * Список карт
 */
export interface CardListWithMeta {
    "items"?: Array<Card>;
    "meta"?: Meta;
}
/**
 * Груз
 */
export interface Cargo {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Срок истекания торгов
     */
    "auctionExpire"?: string;
    /**
     * Начало торгов
     */
    "auctionStart"?: string;
    /**
     * Статус торгов
     */
    "auctionStatus"?: CargoAuctionStatusEnum;
    /**
     * Идентификатор категории
     */
    "categoryId"?: number;
    /**
     * Идентификатор перевозчика
     */
    "contractorId"?: number;
    /**
     * Дата создания
     */
    "createdAt"?: Date;
    /**
     * Идентификатор отправителя
     */
    "customerId"?: number;
    /**
     * Дата начала
     */
    "dateBeginning"?: Date;
    /**
     * Дата окончания
     */
    "dateEnding"?: Date;
    /**
     * Описание
     */
    "description"?: string;
    /**
     * Высота (м)
     */
    "dimensionsHeight"?: number;
    /**
     * Длина (м)
     */
    "dimensionsLength"?: number;
    /**
     * Ширина (м)
     */
    "dimensionsWidth"?: number;
    /**
     * Идентификатор водителя
     */
    "driverId"?: number;
    /**
     * По городу
     */
    "isByTown"?: boolean;
    /**
     * Наименование
     */
    "name"?: string;
    /**
     * Статус оплаты
     */
    "paymentStatus"?: CargoPaymentStatusEnum;
    /**
     * Тип оплаты
     */
    "paymentType"?: CargoPaymentTypeEnum;
    /**
     * Желаемая цена за всю перевозку
     */
    "price"?: number;
    /**
     * Начальный населенный пункт
     */
    "routeBeginning"?: string;
    /**
     * Адрес (полное название)
     */
    "routeBeginningAddress"?: string;
    /**
     * Широта
     */
    "routeBeginningCoordsLat"?: number;
    /**
     * Долгота
     */
    "routeBeginningCoordsLng"?: number;
    /**
     * Количество грузчиков
     */
    "routeBeginningLoaders"?: number;
    /**
     * Название
     */
    "routeBeginningName"?: string;
    /**
     * Конечный населенный пункт
     */
    "routeEnding"?: string;
    /**
     * Адрес (полное название)
     */
    "routeEndingAddress"?: string;
    /**
     * Широта
     */
    "routeEndingCoordsLat"?: number;
    /**
     * Долгота
     */
    "routeEndingCoordsLng"?: number;
    /**
     * Количество грузчиков
     */
    "routeEndingLoaders"?: number;
    /**
     * Название
     */
    "routeEndingName"?: string;
    /**
     * Промежуточные населенные пункты
     */
    "routeIntermediates"?: Array<CargoRouteIntermediates>;
    /**
     * Статус
     */
    "status"?: CargoStatusEnum;
    /**
     * Время начала
     */
    "timeBeginning"?: string;
    /**
     * Объем (куб. м)
     */
    "volume"?: number;
    /**
     * Вес (кг)
     */
    "weight"?: number;
}
export declare type CargoAuctionStatusEnum = "active" | "completed";
export declare type CargoPaymentStatusEnum = "hold" | "not_paid" | "paid";
export declare type CargoPaymentTypeEnum = "card" | "invoice" | "money";
export declare type CargoStatusEnum = "" | "active" | "cancelled" | "completed" | "executed" | "removed";
/**
 * Список грузов
 */
export interface CargoListWithMeta {
    "items"?: Array<Cargo>;
    "meta"?: Meta;
}
/**
 * Сообщение
 */
export interface CargoMessage {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Идентификатор груза
     */
    "cargoId"?: number;
    /**
     * Текст
     */
    "text"?: string;
}
/**
 * Список сообщений
 */
export interface CargoMessageListWithMeta {
    "items"?: Array<CargoMessage>;
    "meta"?: Meta;
}
/**
 * Предложение
 */
export interface CargoOffer {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Идентификатор груза
     */
    "cargoId"?: number;
    /**
     * Идентификатор отклика
     */
    "cargoRequestId"?: number;
    /**
     * Дата создания
     */
    "createdAt"?: Date;
    /**
     * Идентификатор водителя
     */
    "driverId"?: number;
    /**
     * Статус
     */
    "status"?: CargoOfferStatusEnum;
}
export declare type CargoOfferStatusEnum = "awaiting" | "confirmed" | "accepted" | "cancelled" | "closed" | "replaced";
/**
 * Список предложений
 */
export interface CargoOfferListWithMeta {
    "items"?: Array<CargoOffer>;
    "meta"?: Meta;
}
/**
 * Отклик
 */
export interface CargoRequest {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Идентификатор груза
     */
    "cargoId"?: number;
    /**
     * Комментарий
     */
    "comment"?: string;
    /**
     * Дата создания
     */
    "createdAt"?: Date;
    /**
     * По городу
     */
    "isByTown"?: boolean;
    /**
     * Мин. кол-во часов
     */
    "minHours"?: number;
    /**
     * Цена для перевозчика (была отправлена перевозчиком)
     */
    "priceForContractor"?: number;
    /**
     * Цена для перевозчика за 1 час (была отправлена перевозчиком)
     */
    "priceForContractor1Hour"?: number;
    /**
     * Цена для отправителя
     */
    "priceForCustomer"?: number;
    /**
     * Цена для отправителя за 1 час
     */
    "priceForCustomer1Hour"?: number;
    /**
     * Идентификатор пользователя-отправителя отклика
     */
    "requestorId"?: number;
    /**
     * Идентификатор компании-отправителя отклика
     */
    "requestorPersonId"?: number;
    /**
     * Статус
     */
    "status"?: CargoRequestStatusEnum;
    /**
     * Статус от грузоперевозчика
     */
    "statusByContractor"?: CargoRequestStatusByContractorEnum;
    /**
     * Статус от отправителя
     */
    "statusByCustomer"?: CargoRequestStatusByCustomerEnum;
    /**
     * Идентификатор ТС
     */
    "vehicleId"?: number;
    /**
     * Идентификатор типа ТС
     */
    "vehicleTypeId"?: number;
}
export declare type CargoRequestStatusEnum = "accepted" | "active" | "confirmed" | "went" | "arrived" | "loaded" | "shipping_started" | "shipping_finished" | "unloaded" | "completed" | "cancelled" | "replaced";
export declare type CargoRequestStatusByContractorEnum = "active" | "confirmed" | "went" | "arrived" | "loaded" | "shipping_started" | "shipping_finished" | "unloaded" | "completed" | "cancelled";
export declare type CargoRequestStatusByCustomerEnum = "active" | "accepted" | "completed" | "cancelled";
/**
 * Список откликов
 */
export interface CargoRequestListWithMeta {
    "items"?: Array<CargoRequest>;
    "meta"?: Meta;
}
/**
 * Ответственное лицо
 */
export interface CargoResponsiblePerson {
    /**
     * E-mail
     */
    "email"?: string;
    /**
     * Имя
     */
    "firstName"?: string;
    /**
     * Фамилия
     */
    "lastName"?: string;
    /**
     * Отчество
     */
    "middleName"?: string;
    /**
     * ФИО
     */
    "name"?: string;
    /**
     * Телефон
     */
    "phone"?: string;
    /**
     * Тип
     */
    "type"?: CargoResponsiblePersonTypeEnum;
}
export declare type CargoResponsiblePersonTypeEnum = "contractor_responsible_person" | "customer_responsible_person" | "sender" | "recipient";
/**
 * Список ответственных лиц
 */
export interface CargoResponsiblePersonList extends Array<CargoResponsiblePerson> {
}
/**
 * Промежуточный населенный пункт
 */
export interface CargoRouteIntermediates {
    /**
     * Адрес (полное название)
     */
    "address"?: string;
    /**
     * Широта
     */
    "coordsLat"?: number;
    /**
     * Долгота
     */
    "coordsLng"?: number;
    /**
     * Количество грузчиков
     */
    "loaders"?: number;
    /**
     * Название
     */
    "name"?: string;
    /**
     * Путь (код)
     */
    "path"?: string;
}
/**
 * Точка
 */
export interface CargoTrack {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Идентификатор груза
     */
    "cargoId"?: number;
    /**
     * Высота
     */
    "coordsAlt"?: number;
    /**
     * Широта
     */
    "coordsLat"?: number;
    /**
     * Долгота
     */
    "coordsLng"?: number;
    /**
     * Курс
     */
    "course"?: number;
    /**
     * Скорость
     */
    "speed"?: number;
    /**
     * Временная метка
     */
    "timestamp"?: string;
}
/**
 * Список точек
 */
export interface CargoTrackListWithMeta {
    "items"?: Array<CargoTrack>;
    "meta"?: Meta;
}
/**
 * Груз с откликами
 */
export interface CargoWithRequests {
    "cargo"?: Cargo;
    "myRequest"?: CargoRequest;
    "requests"?: Array<CargoRequest>;
}
/**
 * Список грузов с откликами
 */
export interface CargoWithRequestsListWithMeta {
    "items"?: Array<CargoWithRequests>;
    "meta"?: Meta;
}
export interface ConfigList {
    /**
     * Длина пароля в SMS-сообщениях
     */
    "smsMessagePasswordLength"?: number;
}
/**
 * Фактический адрес
 */
export interface DashboardregistrationsubmitAddress {
    /**
     * Адрес
     */
    "address"?: string;
    /**
     * Офис
     */
    "office"?: string;
    /**
     * Абонентский ящик
     */
    "postOfficeBox"?: string;
    /**
     * Почтовый индекс
     */
    "postalIndex"?: number;
}
/**
 * Банковские реквизиты
 */
export interface DashboardregistrationsubmitBankDetails {
    /**
     * Название банка
     */
    "bankName"?: string;
    /**
     * БИК банка
     */
    "bankBik"?: number;
    /**
     * К/С банка
     */
    "bankKs"?: number;
    /**
     * Р/С
     */
    "rs"?: number;
}
/**
 * Юридический адрес
 */
export interface DashboardregistrationsubmitLegalAddress {
    /**
     * Адрес
     */
    "address"?: string;
    /**
     * Офис
     */
    "office"?: string;
    /**
     * Абонентский ящик
     */
    "postOfficeBox"?: string;
    /**
     * Почтовый индекс
     */
    "postalIndex"?: number;
}
/**
 * Паспорт
 */
export interface DashboardregistrationsubmitPassport {
    /**
     * Дата выдачи
     */
    "dateOfIssue"?: Date;
    /**
     * Кем выдан
     */
    "issuer"?: string;
    /**
     * Код подразделения
     */
    "issuerCode"?: string;
    /**
     * Номер
     */
    "number"?: number;
    /**
     * Серия
     */
    "series"?: number;
}
/**
 * ТС
 */
export interface DashboardregistrationsubmitVehicle {
    /**
     * Марка
     */
    "brand"?: string;
    /**
     * Модель
     */
    "brandModel"?: string;
    /**
     * Тип ТС
     */
    "typeId"?: number;
    /**
     * Объем (куб. м)
     */
    "volume"?: number;
    /**
     * Вес (кг)
     */
    "weight"?: number;
}
/**
 * Водитель
 */
export interface Driver {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Ссылка на аватар
     */
    "avatarLink"?: string;
    /**
     * Ссылка на аватар 100x100 пикс.
     */
    "avatarLink100x100"?: string;
    /**
     * Ссылка на аватар 200x200 пикс.
     */
    "avatarLink200x200"?: string;
    /**
     * Идентификатор канала
     */
    "channelId"?: string;
    /**
     * E-mail
     */
    "email"?: string;
    /**
     * Имя
     */
    "firstName"?: string;
    /**
     * Фамилия
     */
    "lastName"?: string;
    /**
     * Отчество
     */
    "middleName"?: string;
    /**
     * Телефон
     */
    "phone"?: string;
}
/**
 * Полный профиль
 */
export interface FullProfile {
    "bankDetails"?: PersonBankDetails;
    "person"?: PersonFullData;
    "profile"?: Profile;
    "vehicle"?: Vehicle;
}
/**
 * Список
 */
export interface List extends Array<ListItem> {
}
/**
 * Объект списка
 */
export interface ListItem {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Название
     */
    "name"?: string;
}
/**
 * Списки
 */
export interface Lists extends Array<ListItem> {
}
export interface LongPoll1 {
    "cargo"?: Cargo;
    "cargoRequest"?: CargoRequest;
    "cargoRequests"?: CargoRequestListWithMeta;
}
export interface LongPoll2 {
    "cargo"?: Cargo;
}
export interface Meta {
    "totalItems"?: number;
    "totalPages"?: number;
}
export interface ModelError {
    "code"?: number;
    "error"?: boolean;
    "message"?: string;
}
/**
 * Транзакция
 */
export interface PaymentTransaction {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Сумма
     */
    "amount"?: number;
    /**
     * Идентификатор груза
     */
    "cargoId"?: number;
    /**
     * Примечание
     */
    "comment"?: string;
    /**
     * Дата и время
     */
    "dateTime"?: string;
    /**
     * Статус
     */
    "status"?: PaymentTransactionStatusEnum;
    /**
     * Тип
     */
    "type"?: PaymentTransactionTypeEnum;
}
export declare type PaymentTransactionStatusEnum = "default" | "cancelled" | "confirmed";
export declare type PaymentTransactionTypeEnum = "incoming" | "outcoming";
/**
 * Баланс
 */
export interface PaymentTransactionBalance {
    /**
     * Баланс
     */
    "balance"?: number;
    /**
     * Заработано
     */
    "incoming"?: number;
    /**
     * Списано
     */
    "outcoming"?: number;
}
/**
 * Список транзакций
 */
export interface PaymentTransactionListWithMeta {
    "items"?: Array<PaymentTransaction>;
    "meta"?: Meta;
}
/**
 * Компания
 */
export interface Person {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Комментарий к откликам
     */
    "commentOnRequests"?: string;
    /**
     * Название (имя)
     */
    "name"?: string;
}
/**
 * Реквизиты
 */
export interface PersonBankDetails {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * БИК банка
     */
    "bankBik"?: string;
    /**
     * К/С банка
     */
    "bankKs"?: string;
    /**
     * Название банка
     */
    "bankName"?: string;
    /**
     * Название
     */
    "name"?: string;
    /**
     * Р/С
     */
    "rs"?: string;
}
/**
 * Компания
 */
export interface PersonFullData {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Тип аккаунта
     */
    "accountType"?: PersonFullDataAccountTypeEnum;
    "address"?: PersonFullDataAddress;
    /**
     * Комментарий к откликам
     */
    "commentOnRequests"?: string;
    /**
     * Данные верны
     */
    "dataIsValid"?: boolean;
    /**
     * Имя
     */
    "firstName"?: string;
    /**
     * ИНН
     */
    "inn"?: string;
    /**
     * По городу
     */
    "isByTown"?: boolean;
    /**
     * Пререгистрация
     */
    "isPreregistration"?: boolean;
    /**
     * С НДС
     */
    "isWithVat"?: boolean;
    /**
     * КПП
     */
    "kpp"?: string;
    /**
     * Фамилия
     */
    "lastName"?: string;
    "legalAddress"?: PersonFullDataLegalAddress;
    /**
     * Отчество
     */
    "middleName"?: string;
    /**
     * Название либо ФИО
     */
    "name"?: string;
    /**
     * ОГРН/ОГРНИП
     */
    "ogrn"?: string;
    "passport"?: PersonFullDataPassport;
    /**
     * Город
     */
    "town"?: string;
    /**
     * Адрес
     */
    "townAddress"?: string;
    /**
     * Название города
     */
    "townName"?: string;
    /**
     * Тип
     */
    "type"?: PersonFullDataTypeEnum;
}
export declare type PersonFullDataAccountTypeEnum = "unknown" | "contractor" | "customer";
export declare type PersonFullDataTypeEnum = "unknown" | "businessman" | "legal_person" | "physical_person";
/**
 * Адрес
 */
export interface PersonFullDataAddress {
    /**
     * Адрес
     */
    "address"?: string;
    /**
     * Офис
     */
    "office"?: string;
    /**
     * А/Я
     */
    "postOfficeBox"?: string;
    /**
     * Почтовый индекс
     */
    "postalIndex"?: string;
}
/**
 * Юр. адрес
 */
export interface PersonFullDataLegalAddress {
    /**
     * Адрес
     */
    "address"?: string;
    /**
     * Офис
     */
    "office"?: string;
    /**
     * А/Я
     */
    "postOfficeBox"?: string;
    /**
     * Почтовый индекс
     */
    "postalIndex"?: string;
}
/**
 * Юр. адрес
 */
export interface PersonFullDataPassport {
    /**
     * Дата выдачи
     */
    "dateOfIssue"?: Date;
    /**
     * Кем выдан
     */
    "issuer"?: string;
    /**
     * Код подразделения
     */
    "issuerCode"?: string;
    /**
     * Номер
     */
    "number"?: string;
    /**
     * Серия
     */
    "series"?: string;
}
/**
 * Список компаний
 */
export interface PersonList extends Array<Person> {
}
/**
 * Список компаний
 */
export interface PersonListWithMeta {
    "items"?: Array<Person>;
    "meta"?: Meta;
}
export interface PersonRegistrationForm {
    /**
     * Тип аккаунта
     */
    "accountType"?: PersonRegistrationFormAccountTypeEnum;
    /**
     * Название
     */
    "name"?: string;
    /**
     * Город
     */
    "town"?: string;
    /**
     * Тип
     */
    "type"?: PersonRegistrationFormTypeEnum;
}
export declare type PersonRegistrationFormAccountTypeEnum = "contractor" | "customer";
export declare type PersonRegistrationFormTypeEnum = "businessman" | "legal_person" | "physical_person";
/**
 * Статус регистрации компании
 */
export interface PersonRegistrationStatus {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Данные
     */
    "dataStatus"?: PersonRegistrationStatusDataStatusEnum;
    /**
     * Сотрудники
     */
    "employeesStatus"?: PersonRegistrationStatusEmployeesStatusEnum;
    /**
     * Идентификатор компании
     */
    "personId"?: number;
    /**
     * Пользовательское соглашение
     */
    "termsOfUseStatus"?: PersonRegistrationStatusTermsOfUseStatusEnum;
    /**
     * Автомобили
     */
    "vehiclesStatus"?: PersonRegistrationStatusVehiclesStatusEnum;
}
export declare type PersonRegistrationStatusDataStatusEnum = "unknown" | "completed" | "skipped";
export declare type PersonRegistrationStatusEmployeesStatusEnum = "unknown" | "completed" | "skipped";
export declare type PersonRegistrationStatusTermsOfUseStatusEnum = "unknown" | "completed" | "skipped";
export declare type PersonRegistrationStatusVehiclesStatusEnum = "unknown" | "completed" | "skipped";
/**
 * Профиль
 */
export interface Profile {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Ссылка на аватар
     */
    "avatarLink"?: string;
    /**
     * Ссылка на аватар 50x50 пикс.
     */
    "avatarLink50x50"?: string;
    /**
     * Ссылка на аватар 200x200 пикс.
     */
    "avatarLink200x200"?: string;
    /**
     * О сотруднике
     */
    "briefAbout"?: string;
    /**
     * E-mail
     */
    "email"?: string;
    /**
     * Имя
     */
    "name"?: string;
    /**
     * Телефон
     */
    "phone"?: string;
}
export interface Success {
    "message"?: string;
    "success"?: boolean;
}
export interface SuccessCreate {
    /**
     * Идентификатор созданного объекта
     */
    "id"?: number;
    "message"?: string;
    "success"?: boolean;
}
export interface SuccessForm {
    /**
     * Данные
     */
    "data"?: Array<string>;
    /**
     * Успех
     */
    "success"?: boolean;
    /**
     * URL
     */
    "url"?: string;
}
export interface SuccessSessionId {
    "message"?: string;
    /**
     * Идентификатор сессии
     */
    "sid"?: string;
    "success"?: boolean;
}
export interface SuccessSpecial1 {
    /**
     * Идентификатор созданного пользователя или для которого требуется подтверждение переноса
     */
    "id"?: number;
    "message"?: string;
    "mode"?: SuccessSpecial1ModeEnum;
    "success"?: boolean;
}
export declare type SuccessSpecial1ModeEnum = "confirmation_required" | "created";
export interface SuccessUrl {
    "success"?: boolean;
    "url"?: string;
}
/**
 * Текст
 */
export interface Text {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Текст
     */
    "text"?: string;
    /**
     * Заголовок
     */
    "title"?: string;
}
/**
 * Загруженный файл
 */
export interface UploadedFile {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Ссылка
     */
    "link"?: string;
    /**
     * Тип
     */
    "type"?: UploadedFileTypeEnum;
}
export declare type UploadedFileTypeEnum = "user-passport-page-1" | "user-passport-page-2" | "vehicle-passport-page-1" | "vehicle-passport-page-2";
/**
 * Список загруженных файлов
 */
export interface UploadedFileListWithMeta {
    "items"?: Array<UploadedFile>;
    "meta"?: Meta;
}
/**
 * Тип загруженного файла
 */
export interface UploadedFileType {
    /**
     * Ключ
     */
    "key"?: string;
    /**
     * Название
     */
    "name"?: string;
}
/**
 * Список типов загруженных файлов
 */
export interface UploadedFileTypeList extends Array<UploadedFileType> {
}
/**
 * Пользователь
 */
export interface User {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Ссылка на аватар
     */
    "avatarLink"?: string;
    /**
     * Ссылка на аватар 50x50 пикс.
     */
    "avatarLink50x50"?: string;
    /**
     * Ссылка на аватар 200x200 пикс.
     */
    "avatarLink200x200"?: string;
    /**
     * О сотруднике
     */
    "briefAbout"?: string;
    /**
     * E-mail
     */
    "email"?: string;
    /**
     * Имя
     */
    "name"?: string;
    /**
     * Телефон
     */
    "phone"?: string;
}
/**
 * Точка
 */
export interface UserGeolocation {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Высота
     */
    "coordsAlt"?: number;
    /**
     * Широта
     */
    "coordsLat"?: number;
    /**
     * Долгота
     */
    "coordsLng"?: number;
    /**
     * Курс
     */
    "course"?: number;
    /**
     * Скорость
     */
    "speed"?: number;
    /**
     * Временная метка
     */
    "timestamp"?: string;
    /**
     * Идентификатор пользователя
     */
    "userId"?: number;
}
/**
 * Список точек
 */
export interface UserGeolocationListWithMeta {
    "items"?: Array<UserGeolocation>;
    "meta"?: Meta;
}
/**
 * Список пользователей
 */
export interface UserList extends Array<User> {
}
export interface UserRegistrationForm {
    /**
     * E-mail
     */
    "email"?: string;
    /**
     * Имя
     */
    "firstName"?: string;
    /**
     * Фамилия
     */
    "lastName"?: string;
    /**
     * Имя
     */
    "name"?: string;
    /**
     * Пароль
     */
    "password"?: string;
    /**
     * Телефон
     */
    "phone"?: string;
}
/**
 * ТС
 */
export interface Vehicle {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * Марка
     */
    "brand"?: string;
    /**
     * Модель
     */
    "brandModel"?: string;
    /**
     * Номер
     */
    "number"?: string;
    /**
     * Объем (куб. м)
     */
    "volume"?: number;
    /**
     * Вес (кг)
     */
    "weight"?: number;
}
/**
 * Список ТС
 */
export interface VehicleList extends Array<Vehicle> {
}
/**
 * Список ТС
 */
export interface VehicleListWithMeta {
    "items"?: Array<Vehicle>;
    "meta"?: Meta;
}
export interface VehicleRegistrationForm {
    /**
     * Марка
     */
    "brand"?: string;
    /**
     * Модель
     */
    "brandModel"?: string;
    /**
     * Номер
     */
    "number"?: string;
    /**
     * Объем (куб. м)
     */
    "volume"?: number;
    /**
     * Вес (кг)
     */
    "weight"?: number;
}
/**
 * Список типов ТС
 */
export interface VehicleTypeList extends Array<VehicleTypeListItem> {
}
/**
 * Объект списка типов ТС
 */
export interface VehicleTypeListItem {
    /**
     * Идентификатор
     */
    "id"?: number;
    /**
     * SVG иконки
     */
    "iconSvg"?: string;
    /**
     * Название
     */
    "name"?: string;
    /**
     * Высота (м)
     */
    "presetDimensionsHeight"?: number;
    /**
     * Длина (м)
     */
    "presetDimensionsLength"?: number;
    /**
     * Ширина (м)
     */
    "presetDimensionsWidth"?: number;
    /**
     * Объем (куб. м)
     */
    "presetVolume"?: number;
    /**
     * Вес (кг)
     */
    "presetWeight"?: number;
}
/**
 * DefaultApi - fetch parameter creator
 */
export declare const DefaultApiFetchParamCreator: {
    autocompleteBankDataGet(params: {
        "bankBik": string;
    }, options?: any): FetchArgs;
    autocompleteCompanyDataGet(params: {
        "inn": string;
    }, options?: any): FetchArgs;
    autocompleteGet(params: {
        "modelAlias": string;
        "q": string;
    }, options?: any): FetchArgs;
    classifierGetBankDataGet(params: {
        "bankBik": string;
    }, options?: any): FetchArgs;
    classifierGetCompanyDataGet(params: {
        "inn": string;
    }, options?: any): FetchArgs;
    classifierGetListModelAliasGet(params: {
        "modelAlias": string;
    }, options?: any): FetchArgs;
    classifierGetListVehicleTypeGet(options?: any): FetchArgs;
    classifierGetListsModelsAliasesGet(params: {
        "modelsAliases": string;
    }, options?: any): FetchArgs;
    configGetListGet(options?: any): FetchArgs;
    dashboardCardBindPost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCardBindWithoutCardPost(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCardCreatePost(params: {
        "cvv": number;
        "expireMonth": number;
        "expireYear": number;
        "number": number;
        "owner": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCardGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCardGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCardRemovePost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCardSavePost(params: {
        "cvv": number;
        "expireMonth": number;
        "expireYear": number;
        "number": number;
        "owner": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCardUnbindPost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoCalculateRecommendedPricePost(params: {
        "hours": number;
        "paymentType": string;
        "routeBeginning": string;
        "routeEnding": string;
        "sid"?: string;
        "vehicleTypeId"?: number;
    }, options?: any): FetchArgs;
    dashboardCargoCreatePost(params: {
        "dateBeginning": Date;
        "dateEnding": Date;
        "dimensionsHeight": number;
        "dimensionsLength": number;
        "dimensionsWidth": number;
        "paymentType": string;
        "routeBeginning": string;
        "routeEnding": string;
        "timeBeginning": string;
        "volume": number;
        "weight": number;
        "sid"?: string;
        "description"?: string;
        "name"?: string;
        "routeIntermediates"?: string[];
        "categoryId"?: number;
        "paymentCardId"?: number;
        "price"?: number;
    }, options?: any): FetchArgs;
    dashboardCargoDriverGetGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoGetListGet(params: {
        "mode"?: string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoGetListWithRequestsGet(params: {
        "dataMode"?: string;
        "mode"?: string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoGetResponsiblePersonsGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoMessageGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoMessageSubmitPost(params: {
        "cargoId": number;
        "text": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestContractorAddPost(params: {
        "cargoId": number;
        "extraHourPrice": number;
        "minHours": number;
        "price": number;
        "vehicleId": number;
        "sid"?: string;
        "comment"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestContractorCancelPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestContractorCompletePost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestContractorConfirmPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestContractorLoadPost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestContractorWentPost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestCustomerAcceptPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestCustomerCancelPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestCustomerCompletePost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoRequestGetListGet_1(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoTrackGetLastGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoTrackGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardCargoTrackSubmitPost(params: {
        "cargoId": number;
        "coordsLat": number;
        "coordsLng": number;
        "sid"?: string;
        "coordsAlt"?: number;
        "course"?: number;
        "speed"?: number;
    }, options?: any): FetchArgs;
    dashboardCargoVehicleGetGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardGeolocationGetLastGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardGeolocationGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardGeolocationSubmitPost(params: {
        "coordsLat": number;
        "coordsLng": number;
        "sid"?: string;
        "coordsAlt"?: number;
        "course"?: number;
        "speed"?: number;
    }, options?: any): FetchArgs;
    dashboardPaymentTransactionGetBalanceGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardPaymentTransactionGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardPaymentTransactionSubmitPayoutPost(params: {
        "amount": number;
        "cardNumber": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardPaymentTransactionSubmitRequestPost(params: {
        "amount": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardPersonGetGet(params: {
        "personId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardPersonGetListByIdsGet(params: {
        "personsIds": string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardPersonGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardProfileGetFullGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardProfileGetGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardProfileUpdatePost(params: {
        "email": string;
        "phone": string;
        "name": string;
        "sid"?: string;
        "briefAbout"?: string;
    }, options?: any): FetchArgs;
    dashboardPushSubscribePost(params: {
        "userId": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardPushUnsubscribePost(params: {
        "userId": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2DataSaveBusinessmanPost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "bankBik"?: number;
        "bankKs"?: number;
        "bankName"?: string;
        "inn"?: number;
        "legalAddressFull"?: string;
        "legalAddressOffice"?: string;
        "legalAddressPostOfficeBox"?: string;
        "legalAddressPostalIndex"?: number;
        "ogrn"?: number;
        "rs"?: number;
        "taxationTypeId"?: number;
        "town"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2DataSaveLegalPersonPost(params: {
        "name": string;
        "sid"?: string;
        "bankBik"?: number;
        "bankKs"?: number;
        "bankName"?: string;
        "flagWithVat"?: number;
        "inn"?: number;
        "kpp"?: number;
        "legalAddressFull"?: string;
        "legalAddressOffice"?: string;
        "legalAddressPostOfficeBox"?: string;
        "legalAddressPostalIndex"?: number;
        "ogrn"?: number;
        "rs"?: number;
        "taxationTypeId"?: number;
        "town"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2DataSavePhysicalPersonPost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "town"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2DataUploadFilePost(params: {
        "file": string;
        "type": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2EmployeeConfirmPost(params: {
        "id": number;
        "code": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2EmployeeCreatePost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "sid"?: string;
        "email"?: string;
        "roles"?: string[];
    }, options?: any): FetchArgs;
    dashboardRegistration2EmployeeDeletePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2EmployeeGetListGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2EmployeeNextPost(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2EmployeeUpdatePost(params: {
        "firstName": string;
        "id": number;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "roles"?: string[];
    }, options?: any): FetchArgs;
    dashboardRegistration2EmployeeUploadFilePost(params: {
        "file": string;
        "type": string;
        "userId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2GetStatusGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2TermsOfUseGetTextGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2TermsOfUseReadPost(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2TermsOfUseSkipPost(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2VehicleCreatePost(params: {
        "bodyUsefulHeight": number;
        "bodyUsefulLength": number;
        "bodyUsefulWidth": number;
        "weight": number;
        "sid"?: string;
        "employeeDriverId"?: number;
        "vehicleBodyTypeId"?: number;
        "ownerLegalPersonName"?: string;
        "ownerNaturalPersonFirstName"?: string;
        "ownerNaturalPersonLastName"?: string;
        "ownerNaturalPersonMiddleName"?: string;
        "ownerType"?: string;
        "vehiclesPropsIds"?: number[];
        "vehicleTypeId"?: number;
        "volume"?: number;
    }, options?: any): FetchArgs;
    dashboardRegistration2VehicleDeletePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2VehicleGetListGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2VehicleNextPost(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistration2VehicleUpdatePost(params: {
        "id": number;
        "bodyUsefulHeight": number;
        "bodyUsefulLength": number;
        "bodyUsefulWidth": number;
        "weight": number;
        "sid"?: string;
        "employeeDriverId"?: number;
        "vehicleBodyTypeId"?: number;
        "ownerLegalPersonName"?: string;
        "ownerNaturalPersonFirstName"?: string;
        "ownerNaturalPersonLastName"?: string;
        "ownerNaturalPersonMiddleName"?: string;
        "ownerType"?: string;
        "vehiclesPropsIds"?: number[];
        "vehicleTypeId"?: number;
        "volume"?: number;
    }, options?: any): FetchArgs;
    dashboardRegistration2VehicleUploadFilePost(params: {
        "file": string;
        "type": string;
        "vehicleId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistrationGetUploadedFilesGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistrationGetUploadedFilesTypesGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistrationRemoveUploadedFilePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardRegistrationSubmitPost(params: {
        "sid"?: string;
        "body"?: Body;
    }, options?: any): FetchArgs;
    dashboardRegistrationUploadFilePost(params: {
        "file": string;
        "type": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardVehicleCreatePost(params: {
        "volume": number;
        "weight": number;
        "sid"?: string;
        "brand"?: string;
        "brandModel"?: string;
        "number"?: string;
    }, options?: any): FetchArgs;
    dashboardVehicleGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardVehicleGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardVehicleRemovePost(params: {
        "vehicleId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    dashboardVehicleUpdatePost(params: {
        "vehicleId": number;
        "volume": number;
        "weight": number;
        "sid"?: string;
        "brand"?: string;
        "brandModel"?: string;
        "number"?: string;
    }, options?: any): FetchArgs;
    driverAuthorizationLoginConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverAuthorizationLoginPost(params: {
        "phone": string;
    }, options?: any): FetchArgs;
    driverAuthorizationLogoutPost(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoMessageGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoMessageSubmitPost(params: {
        "cargoId": number;
        "text": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoOfferCancelPost(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoOfferConfirmPost(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoOfferGetGet(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoOfferGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoRequestCompletePost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoRequestGetGet(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoRequestGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoRequestLoadPost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardCargoRequestWentPost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardPaymentTransactionGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardPaymentTransactionSubmitRequestPost(params: {
        "amount": number;
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardProfileGetGet(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    driverDashboardProfileUpdatePost(params: {
        "email": string;
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    userAuthorizationCheckPhonePost(params: {
        "phone": string;
    }, options?: any): FetchArgs;
    userAuthorizationLoginByEmailPost(params: {
        "email": string;
        "password": string;
    }, options?: any): FetchArgs;
    userAuthorizationLoginByPhoneConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    userAuthorizationLoginByPhonePost(params: {
        "phone": string;
    }, options?: any): FetchArgs;
    userAuthorizationLogoutPost(params: {
        "sid"?: string;
    }, options?: any): FetchArgs;
    userRegistration2RegisterConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    userRegistration2RegisterPost(params: {
        "accountSubtype": string;
        "accountType": string;
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "email"?: string;
    }, options?: any): FetchArgs;
    userRegistrationRegisterConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): FetchArgs;
    userRegistrationRegisterPost(params: {
        "body"?: Body1;
    }, options?: any): FetchArgs;
};
/**
 * DefaultApi - functional programming interface
 */
export declare const DefaultApiFp: {
    autocompleteBankDataGet(params: {
        "bankBik": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<AutocompleteBankDataList>;
    autocompleteCompanyDataGet(params: {
        "inn": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<AutocompleteCompanyDataList>;
    autocompleteGet(params: {
        "modelAlias": string;
        "q": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<AutocompleteList>;
    classifierGetBankDataGet(params: {
        "bankBik": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<AutocompleteBankData>;
    classifierGetCompanyDataGet(params: {
        "inn": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<AutocompleteCompanyData>;
    classifierGetListModelAliasGet(params: {
        "modelAlias": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<ListItem[]>;
    classifierGetListVehicleTypeGet(options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<VehicleTypeList>;
    classifierGetListsModelsAliasesGet(params: {
        "modelsAliases": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Lists>;
    configGetListGet(options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<ConfigList>;
    dashboardCardBindPost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessUrl>;
    dashboardCardBindWithoutCardPost(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessForm>;
    dashboardCardCreatePost(params: {
        "cvv": number;
        "expireMonth": number;
        "expireYear": number;
        "number": number;
        "owner": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardCardGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Card>;
    dashboardCardGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CardListWithMeta>;
    dashboardCardRemovePost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCardSavePost(params: {
        "cvv": number;
        "expireMonth": number;
        "expireYear": number;
        "number": number;
        "owner": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessUrl>;
    dashboardCardUnbindPost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoCalculateRecommendedPricePost(params: {
        "hours": number;
        "paymentType": string;
        "routeBeginning": string;
        "routeEnding": string;
        "sid"?: string;
        "vehicleTypeId"?: number;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CalculatedRecommendedPrice>;
    dashboardCargoCreatePost(params: {
        "dateBeginning": Date;
        "dateEnding": Date;
        "dimensionsHeight": number;
        "dimensionsLength": number;
        "dimensionsWidth": number;
        "paymentType": string;
        "routeBeginning": string;
        "routeEnding": string;
        "timeBeginning": string;
        "volume": number;
        "weight": number;
        "sid"?: string;
        "description"?: string;
        "name"?: string;
        "routeIntermediates"?: string[];
        "categoryId"?: number;
        "paymentCardId"?: number;
        "price"?: number;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardCargoDriverGetGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Driver>;
    dashboardCargoGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Cargo>;
    dashboardCargoGetListGet(params: {
        "mode"?: string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoListWithMeta>;
    dashboardCargoGetListWithRequestsGet(params: {
        "dataMode"?: string;
        "mode"?: string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoWithRequestsListWithMeta>;
    dashboardCargoGetResponsiblePersonsGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoResponsiblePersonList>;
    dashboardCargoMessageGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoMessageListWithMeta>;
    dashboardCargoMessageSubmitPost(params: {
        "cargoId": number;
        "text": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestContractorAddPost(params: {
        "cargoId": number;
        "extraHourPrice": number;
        "minHours": number;
        "price": number;
        "vehicleId": number;
        "sid"?: string;
        "comment"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestContractorCancelPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestContractorCompletePost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestContractorConfirmPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestContractorLoadPost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestContractorWentPost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestCustomerAcceptPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestCustomerCancelPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestCustomerCompletePost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoRequestGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoRequest>;
    dashboardCargoRequestGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoRequestListWithMeta>;
    dashboardCargoRequestGetListGet_1(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoRequestListWithMeta>;
    dashboardCargoTrackGetLastGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoTrack>;
    dashboardCargoTrackGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoTrackListWithMeta>;
    dashboardCargoTrackSubmitPost(params: {
        "cargoId": number;
        "coordsLat": number;
        "coordsLng": number;
        "sid"?: string;
        "coordsAlt"?: number;
        "course"?: number;
        "speed"?: number;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardCargoVehicleGetGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Vehicle>;
    dashboardGeolocationGetLastGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<UserGeolocation>;
    dashboardGeolocationGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<UserGeolocationListWithMeta>;
    dashboardGeolocationSubmitPost(params: {
        "coordsLat": number;
        "coordsLng": number;
        "sid"?: string;
        "coordsAlt"?: number;
        "course"?: number;
        "speed"?: number;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardPaymentTransactionGetBalanceGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<PaymentTransactionBalance>;
    dashboardPaymentTransactionGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<PaymentTransactionListWithMeta>;
    dashboardPaymentTransactionSubmitPayoutPost(params: {
        "amount": number;
        "cardNumber": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardPaymentTransactionSubmitRequestPost(params: {
        "amount": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardPersonGetGet(params: {
        "personId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Person>;
    dashboardPersonGetListByIdsGet(params: {
        "personsIds": string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<PersonList>;
    dashboardPersonGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<PersonListWithMeta>;
    dashboardProfileGetFullGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<FullProfile>;
    dashboardProfileGetGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Profile>;
    dashboardProfileUpdatePost(params: {
        "email": string;
        "phone": string;
        "name": string;
        "sid"?: string;
        "briefAbout"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardPushSubscribePost(params: {
        "userId": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardPushUnsubscribePost(params: {
        "userId": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2DataSaveBusinessmanPost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "bankBik"?: number;
        "bankKs"?: number;
        "bankName"?: string;
        "inn"?: number;
        "legalAddressFull"?: string;
        "legalAddressOffice"?: string;
        "legalAddressPostOfficeBox"?: string;
        "legalAddressPostalIndex"?: number;
        "ogrn"?: number;
        "rs"?: number;
        "taxationTypeId"?: number;
        "town"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2DataSaveLegalPersonPost(params: {
        "name": string;
        "sid"?: string;
        "bankBik"?: number;
        "bankKs"?: number;
        "bankName"?: string;
        "flagWithVat"?: number;
        "inn"?: number;
        "kpp"?: number;
        "legalAddressFull"?: string;
        "legalAddressOffice"?: string;
        "legalAddressPostOfficeBox"?: string;
        "legalAddressPostalIndex"?: number;
        "ogrn"?: number;
        "rs"?: number;
        "taxationTypeId"?: number;
        "town"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2DataSavePhysicalPersonPost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "town"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2DataUploadFilePost(params: {
        "file": string;
        "type": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardRegistration2EmployeeConfirmPost(params: {
        "id": number;
        "code": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2EmployeeCreatePost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "sid"?: string;
        "email"?: string;
        "roles"?: string[];
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessSpecial1>;
    dashboardRegistration2EmployeeDeletePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2EmployeeGetListGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<UserList>;
    dashboardRegistration2EmployeeNextPost(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2EmployeeUpdatePost(params: {
        "firstName": string;
        "id": number;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "roles"?: string[];
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2EmployeeUploadFilePost(params: {
        "file": string;
        "type": string;
        "userId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardRegistration2GetStatusGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<PersonRegistrationStatus>;
    dashboardRegistration2TermsOfUseGetTextGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Text>;
    dashboardRegistration2TermsOfUseReadPost(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2TermsOfUseSkipPost(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2VehicleCreatePost(params: {
        "bodyUsefulHeight": number;
        "bodyUsefulLength": number;
        "bodyUsefulWidth": number;
        "weight": number;
        "sid"?: string;
        "employeeDriverId"?: number;
        "vehicleBodyTypeId"?: number;
        "ownerLegalPersonName"?: string;
        "ownerNaturalPersonFirstName"?: string;
        "ownerNaturalPersonLastName"?: string;
        "ownerNaturalPersonMiddleName"?: string;
        "ownerType"?: string;
        "vehiclesPropsIds"?: number[];
        "vehicleTypeId"?: number;
        "volume"?: number;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardRegistration2VehicleDeletePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2VehicleGetListGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<VehicleList>;
    dashboardRegistration2VehicleNextPost(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistration2VehicleUpdatePost(params: {
        "id": number;
        "bodyUsefulHeight": number;
        "bodyUsefulLength": number;
        "bodyUsefulWidth": number;
        "weight": number;
        "sid"?: string;
        "employeeDriverId"?: number;
        "vehicleBodyTypeId"?: number;
        "ownerLegalPersonName"?: string;
        "ownerNaturalPersonFirstName"?: string;
        "ownerNaturalPersonLastName"?: string;
        "ownerNaturalPersonMiddleName"?: string;
        "ownerType"?: string;
        "vehiclesPropsIds"?: number[];
        "vehicleTypeId"?: number;
        "volume"?: number;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardRegistration2VehicleUploadFilePost(params: {
        "file": string;
        "type": string;
        "vehicleId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardRegistrationGetUploadedFilesGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<UploadedFileListWithMeta>;
    dashboardRegistrationGetUploadedFilesTypesGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<UploadedFileTypeList>;
    dashboardRegistrationRemoveUploadedFilePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistrationSubmitPost(params: {
        "sid"?: string;
        "body"?: Body;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardRegistrationUploadFilePost(params: {
        "file": string;
        "type": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardVehicleCreatePost(params: {
        "volume": number;
        "weight": number;
        "sid"?: string;
        "brand"?: string;
        "brandModel"?: string;
        "number"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessCreate>;
    dashboardVehicleGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Vehicle>;
    dashboardVehicleGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<VehicleListWithMeta>;
    dashboardVehicleRemovePost(params: {
        "vehicleId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    dashboardVehicleUpdatePost(params: {
        "vehicleId": number;
        "volume": number;
        "weight": number;
        "sid"?: string;
        "brand"?: string;
        "brandModel"?: string;
        "number"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverAuthorizationLoginConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverAuthorizationLoginPost(params: {
        "phone": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessSessionId>;
    driverAuthorizationLogoutPost(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverDashboardCargoMessageGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoMessageListWithMeta>;
    driverDashboardCargoMessageSubmitPost(params: {
        "cargoId": number;
        "text": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverDashboardCargoOfferCancelPost(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverDashboardCargoOfferConfirmPost(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverDashboardCargoOfferGetGet(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoOffer>;
    driverDashboardCargoOfferGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoOfferListWithMeta>;
    driverDashboardCargoRequestCompletePost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverDashboardCargoRequestGetGet(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoRequest>;
    driverDashboardCargoRequestGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<CargoRequestListWithMeta>;
    driverDashboardCargoRequestLoadPost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverDashboardCargoRequestWentPost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverDashboardPaymentTransactionGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<PaymentTransactionListWithMeta>;
    driverDashboardPaymentTransactionSubmitRequestPost(params: {
        "amount": number;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    driverDashboardProfileGetGet(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Driver>;
    driverDashboardProfileUpdatePost(params: {
        "email": string;
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    userAuthorizationCheckPhonePost(params: {
        "phone": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    userAuthorizationLoginByEmailPost(params: {
        "email": string;
        "password": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessSessionId>;
    userAuthorizationLoginByPhoneConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    userAuthorizationLoginByPhonePost(params: {
        "phone": string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessSessionId>;
    userAuthorizationLogoutPost(params: {
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    userRegistration2RegisterConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    userRegistration2RegisterPost(params: {
        "accountSubtype": string;
        "accountType": string;
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "email"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessSessionId>;
    userRegistrationRegisterConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<Success>;
    userRegistrationRegisterPost(params: {
        "body"?: Body1;
    }, options?: any): (fetch?: FetchAPI, basePath?: string) => Promise<SuccessSessionId>;
};
/**
 * DefaultApi - object-oriented interface
 */
export declare class DefaultApi extends BaseAPI {
    /**
     * Автокомплит банков
     * @param bankBik БИК банка
     */
    autocompleteBankDataGet(params: {
        "bankBik": string;
    }, options?: any): Promise<AutocompleteBankDataList>;
    /**
     * Автокомплит компаний
     * @param inn ИНН
     */
    autocompleteCompanyDataGet(params: {
        "inn": string;
    }, options?: any): Promise<AutocompleteCompanyDataList>;
    /**
     * Автокомплит
     * @param modelAlias Модель
     * @param q Запрос
     */
    autocompleteGet(params: {
        "modelAlias": string;
        "q": string;
    }, options?: any): Promise<AutocompleteList>;
    /**
     * Получить данные банка
     * @param bankBik БИК банка
     */
    classifierGetBankDataGet(params: {
        "bankBik": string;
    }, options?: any): Promise<AutocompleteBankData>;
    /**
     * Получить данные компании
     * @param inn ИНН
     */
    classifierGetCompanyDataGet(params: {
        "inn": string;
    }, options?: any): Promise<AutocompleteCompanyData>;
    /**
     * Получить список классификатора
     * @param modelAlias Классификатор
     */
    classifierGetListModelAliasGet(params: {
        "modelAlias": string;
    }, options?: any): Promise<ListItem[]>;
    /**
     * Получить список типов ТС
     */
    classifierGetListVehicleTypeGet(options?: any): Promise<VehicleTypeList>;
    /**
     * Получить список классификаторов
     * @param modelsAliases Классификаторы через запятую
     */
    classifierGetListsModelsAliasesGet(params: {
        "modelsAliases": string;
    }, options?: any): Promise<Lists>;
    /**
     * Получить список настроек
     */
    configGetListGet(options?: any): Promise<ConfigList>;
    /**
     * Привязать карту
     * @param cardId Идентификатор карты
     * @param sid Идентификатор сессии
     */
    dashboardCardBindPost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): Promise<SuccessUrl>;
    /**
     * Привязать карту
     * @param sid Идентификатор сессии
     */
    dashboardCardBindWithoutCardPost(params: {
        "sid"?: string;
    }, options?: any): Promise<SuccessForm>;
    /**
     * Создать карту
     * @param cvv CVV
     * @param expireMonth Месяц окончания
     * @param expireYear Год окончания
     * @param number Номер
     * @param owner Владелец
     * @param sid Идентификатор сессии
     */
    dashboardCardCreatePost(params: {
        "cvv": number;
        "expireMonth": number;
        "expireYear": number;
        "number": number;
        "owner": string;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Получить карту
     * @param id Идентификатор объекта
     * @param sid Идентификатор сессии
     */
    dashboardCardGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Card>;
    /**
     * Получить список карт
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardCardGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CardListWithMeta>;
    /**
     * Удалить карту
     * @param cardId Идентификатор карты
     * @param sid Идентификатор сессии
     */
    dashboardCardRemovePost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Создать карту
     * @param cvv CVV
     * @param expireMonth Месяц окончания
     * @param expireYear Год окончания
     * @param number Номер
     * @param owner Владелец
     * @param sid Идентификатор сессии
     */
    dashboardCardSavePost(params: {
        "cvv": number;
        "expireMonth": number;
        "expireYear": number;
        "number": number;
        "owner": string;
        "sid"?: string;
    }, options?: any): Promise<SuccessUrl>;
    /**
     * Удалить привязку карты
     * @param cardId Идентификатор карты
     * @param sid Идентификатор сессии
     */
    dashboardCardUnbindPost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Рассчитать цену
     * @param hours Часы
     * @param paymentType Тип оплаты
     * @param routeBeginning Начальный населенный пункт
     * @param routeEnding Конечный населенный пункт
     * @param sid Идентификатор сессии
     * @param vehicleTypeId Идентификатор типа ТС
     */
    dashboardCargoCalculateRecommendedPricePost(params: {
        "hours": number;
        "paymentType": string;
        "routeBeginning": string;
        "routeEnding": string;
        "sid"?: string;
        "vehicleTypeId"?: number;
    }, options?: any): Promise<CalculatedRecommendedPrice>;
    /**
     * Создать груз
     * @param dateBeginning Дата начала
     * @param dateEnding Дата окончания
     * @param dimensionsHeight Высота (м)
     * @param dimensionsLength Длина (м)
     * @param dimensionsWidth Ширина (м)
     * @param paymentType Тип оплаты
     * @param routeBeginning Начальный населенный пункт
     * @param routeEnding Конечный населенный пункт
     * @param timeBeginning Время начала
     * @param volume Объем (куб. м)
     * @param weight Вес (кг)
     * @param sid Идентификатор сессии
     * @param description Описание
     * @param name Наименование
     * @param routeIntermediates Промежуточные населенные пункты
     * @param categoryId Идентификатор категории
     * @param paymentCardId Идентификатор карты
     * @param price Желаемая цена за всю перевозку
     */
    dashboardCargoCreatePost(params: {
        "dateBeginning": Date;
        "dateEnding": Date;
        "dimensionsHeight": number;
        "dimensionsLength": number;
        "dimensionsWidth": number;
        "paymentType": string;
        "routeBeginning": string;
        "routeEnding": string;
        "timeBeginning": string;
        "volume": number;
        "weight": number;
        "sid"?: string;
        "description"?: string;
        "name"?: string;
        "routeIntermediates"?: Array<string>;
        "categoryId"?: number;
        "paymentCardId"?: number;
        "price"?: number;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Получить водителя
     * @param cargoId Идентификатор груза
     * @param sid Идентификатор сессии
     */
    dashboardCargoDriverGetGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Driver>;
    /**
     * Получить груз
     * @param id Идентификатор объекта
     * @param sid Идентификатор сессии
     */
    dashboardCargoGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Cargo>;
    /**
     * Получить список грузов
     * @param mode Режим
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardCargoGetListGet(params: {
        "mode"?: string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoListWithMeta>;
    /**
     * Получить список грузов с откликами
     * @param dataMode Режим данных
     * @param mode Режим
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardCargoGetListWithRequestsGet(params: {
        "dataMode"?: string;
        "mode"?: string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoWithRequestsListWithMeta>;
    /**
     * Получить список ответственных лиц
     * @param cargoId Идентификатор груза
     * @param sid Идентификатор сессии
     */
    dashboardCargoGetResponsiblePersonsGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<CargoResponsiblePersonList>;
    /**
     * Получить список сообщений
     * @param cargoId Идентификатор груза
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardCargoMessageGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoMessageListWithMeta>;
    /**
     * Отправить сообщение
     * @param cargoId Идентификатор груза
     * @param text Текст
     * @param sid Идентификатор сессии
     */
    dashboardCargoMessageSubmitPost(params: {
        "cargoId": number;
        "text": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отправить отклик
     * @param cargoId Идентификатор груза
     * @param extraHourPrice Цена за дополнительный час
     * @param minHours Минимальное количество часов
     * @param price Цена
     * @param vehicleId Идентификатор ТС
     * @param sid Идентификатор сессии
     * @param comment Комментарий
     */
    dashboardCargoRequestContractorAddPost(params: {
        "cargoId": number;
        "extraHourPrice": number;
        "minHours": number;
        "price": number;
        "vehicleId": number;
        "sid"?: string;
        "comment"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отменить отклик
     * @param cargoId Идентификатор груза
     * @param cargoRequestId Идентификатор отклика
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestContractorCancelPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Завершить перевозку груза
     * @param cargoId Идентификатор груза
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestContractorCompletePost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Подтвердить отклик
     * @param cargoId Идентификатор груза
     * @param cargoRequestId Идентификатор отклика
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestContractorConfirmPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Начать перевозку груза
     * @param cargoId Идентификатор груза
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestContractorLoadPost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отправиться за грузом
     * @param cargoId Идентификатор груза
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestContractorWentPost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Принять отклик
     * @param cargoId Идентификатор груза
     * @param cargoRequestId Идентификатор отклика
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestCustomerAcceptPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отменить отклик
     * @param cargoId Идентификатор груза
     * @param cargoRequestId Идентификатор отклика
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestCustomerCancelPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Подтвердить завершение перевозки груза
     * @param cargoId Идентификатор груза
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestCustomerCompletePost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить отклик
     * @param id Идентификатор объекта
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequest>;
    /**
     * Получить список откликов
     * @param cargoId Идентификатор груза
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequestListWithMeta>;
    /**
     * Получить список откликов
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardCargoRequestGetListGet_1(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequestListWithMeta>;
    /**
     * Получить последнюю точку
     * @param cargoId Идентификатор груза
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardCargoTrackGetLastGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoTrack>;
    /**
     * Получить список точек
     * @param cargoId Идентификатор груза
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardCargoTrackGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoTrackListWithMeta>;
    /**
     * Отправить точку
     * @param cargoId Идентификатор груза
     * @param coordsLat Широта
     * @param coordsLng Долгота
     * @param sid Идентификатор сессии
     * @param coordsAlt Высота
     * @param course Курс
     * @param speed Скорость
     */
    dashboardCargoTrackSubmitPost(params: {
        "cargoId": number;
        "coordsLat": number;
        "coordsLng": number;
        "sid"?: string;
        "coordsAlt"?: number;
        "course"?: number;
        "speed"?: number;
    }, options?: any): Promise<Success>;
    /**
     * Получить ТС
     * @param cargoId Идентификатор груза
     * @param sid Идентификатор сессии
     */
    dashboardCargoVehicleGetGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Vehicle>;
    /**
     * Получить последнюю точку
     * @param sid Идентификатор сессии
     */
    dashboardGeolocationGetLastGet(params: {
        "sid"?: string;
    }, options?: any): Promise<UserGeolocation>;
    /**
     * Получить список точек
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardGeolocationGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<UserGeolocationListWithMeta>;
    /**
     * Отправить точку
     * @param coordsLat Широта
     * @param coordsLng Долгота
     * @param sid Идентификатор сессии
     * @param coordsAlt Высота
     * @param course Курс
     * @param speed Скорость
     */
    dashboardGeolocationSubmitPost(params: {
        "coordsLat": number;
        "coordsLng": number;
        "sid"?: string;
        "coordsAlt"?: number;
        "course"?: number;
        "speed"?: number;
    }, options?: any): Promise<Success>;
    /**
     * Получить баланс
     * @param sid Идентификатор сессии
     */
    dashboardPaymentTransactionGetBalanceGet(params: {
        "sid"?: string;
    }, options?: any): Promise<PaymentTransactionBalance>;
    /**
     * Получить список транзакций
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardPaymentTransactionGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<PaymentTransactionListWithMeta>;
    /**
     * Отправить заявку на списание средств
     * @param amount Сумма
     * @param cardNumber Номер карты
     * @param sid Идентификатор сессии
     */
    dashboardPaymentTransactionSubmitPayoutPost(params: {
        "amount": number;
        "cardNumber": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отправить заявку на списание средств
     * @param amount Сумма
     * @param sid Идентификатор сессии
     */
    dashboardPaymentTransactionSubmitRequestPost(params: {
        "amount": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить компанию
     * @param personId Идентификатор компании
     * @param sid Идентификатор сессии
     */
    dashboardPersonGetGet(params: {
        "personId": number;
        "sid"?: string;
    }, options?: any): Promise<Person>;
    /**
     * Получить список компаний по идентификаторам
     * @param personsIds Идентификаторы компаний (через запятую)
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardPersonGetListByIdsGet(params: {
        "personsIds": string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<PersonList>;
    /**
     * Получить список компаний
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardPersonGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<PersonListWithMeta>;
    /**
     * Получить полный профиль
     * @param sid Идентификатор сессии
     */
    dashboardProfileGetFullGet(params: {
        "sid"?: string;
    }, options?: any): Promise<FullProfile>;
    /**
     * Получить профиль
     * @param sid Идентификатор сессии
     */
    dashboardProfileGetGet(params: {
        "sid"?: string;
    }, options?: any): Promise<Profile>;
    /**
     * Обновить профиль
     * @param email E-mail
     * @param phone Телефон
     * @param name Имя
     * @param sid Идентификатор сессии
     * @param briefAbout О сотруднике
     */
    dashboardProfileUpdatePost(params: {
        "email": string;
        "phone": string;
        "name": string;
        "sid"?: string;
        "briefAbout"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Подписать на push-уведомления
     * @param userId Идентификатор пользователя
     * @param sid Идентификатор сессии
     */
    dashboardPushSubscribePost(params: {
        "userId": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отписать от push-уведомлений
     * @param userId Идентификатор пользователя
     * @param sid Идентификатор сессии
     */
    dashboardPushUnsubscribePost(params: {
        "userId": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Сохранить данные (ИП)
     * @param firstName Имя
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param sid Идентификатор сессии
     * @param bankBik БИК банка
     * @param bankKs К/С банка
     * @param bankName Название банка
     * @param inn ИНН
     * @param legalAddressFull Адрес
     * @param legalAddressOffice Офис
     * @param legalAddressPostOfficeBox Абонентский ящик
     * @param legalAddressPostalIndex Почтовый индекс
     * @param ogrn ОГРН
     * @param rs Р/С
     * @param taxationTypeId Тип налогообложения
     * @param town Город
     */
    dashboardRegistration2DataSaveBusinessmanPost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "bankBik"?: number;
        "bankKs"?: number;
        "bankName"?: string;
        "inn"?: number;
        "legalAddressFull"?: string;
        "legalAddressOffice"?: string;
        "legalAddressPostOfficeBox"?: string;
        "legalAddressPostalIndex"?: number;
        "ogrn"?: number;
        "rs"?: number;
        "taxationTypeId"?: number;
        "town"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Сохранить данные (юрлицо)
     * @param name Наименование
     * @param sid Идентификатор сессии
     * @param bankBik БИК банка
     * @param bankKs К/С банка
     * @param bankName Название банка
     * @param flagWithVat НДС
     * @param inn ИНН
     * @param kpp КПП
     * @param legalAddressFull Адрес
     * @param legalAddressOffice Офис
     * @param legalAddressPostOfficeBox Абонентский ящик
     * @param legalAddressPostalIndex Почтовый индекс
     * @param ogrn ОГРН
     * @param rs Р/С
     * @param taxationTypeId Тип налогообложения
     * @param town Город
     */
    dashboardRegistration2DataSaveLegalPersonPost(params: {
        "name": string;
        "sid"?: string;
        "bankBik"?: number;
        "bankKs"?: number;
        "bankName"?: string;
        "flagWithVat"?: number;
        "inn"?: number;
        "kpp"?: number;
        "legalAddressFull"?: string;
        "legalAddressOffice"?: string;
        "legalAddressPostOfficeBox"?: string;
        "legalAddressPostalIndex"?: number;
        "ogrn"?: number;
        "rs"?: number;
        "taxationTypeId"?: number;
        "town"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Сохранить данные (физлицо)
     * @param firstName Имя
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param sid Идентификатор сессии
     * @param town Город
     */
    dashboardRegistration2DataSavePhysicalPersonPost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "town"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Загрузить файл
     * @param file Файл
     * @param type Тип файла
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2DataUploadFilePost(params: {
        "file": string;
        "type": string;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Подтвердить перенос сотрудника
     * @param id Идентификатор
     * @param code Код подтверждения телефона
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2EmployeeConfirmPost(params: {
        "id": number;
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Обновить сотрудника
     * @param firstName Имя
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param phone Телефон
     * @param sid Идентификатор сессии
     * @param email E-mail
     * @param roles Роли пользователя
     */
    dashboardRegistration2EmployeeCreatePost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "sid"?: string;
        "email"?: string;
        "roles"?: Array<string>;
    }, options?: any): Promise<SuccessSpecial1>;
    /**
     * Удалить пользователя
     * @param id Идентификатор
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2EmployeeDeletePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить список сотрудников
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2EmployeeGetListGet(params: {
        "sid"?: string;
    }, options?: any): Promise<UserList>;
    /**
     * Следующее
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2EmployeeNextPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Обновить сотрудника
     * @param firstName Имя
     * @param id Идентификатор
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param sid Идентификатор сессии
     * @param roles Роли пользователя
     */
    dashboardRegistration2EmployeeUpdatePost(params: {
        "firstName": string;
        "id": number;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "roles"?: Array<string>;
    }, options?: any): Promise<Success>;
    /**
     * Загрузить файл
     * @param file Файл
     * @param type Тип файла
     * @param userId Идентификатор пользователя
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2EmployeeUploadFilePost(params: {
        "file": string;
        "type": string;
        "userId": number;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Получить статус регистрации
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2GetStatusGet(params: {
        "sid"?: string;
    }, options?: any): Promise<PersonRegistrationStatus>;
    /**
     * Получить текст пользовательского соглашения
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2TermsOfUseGetTextGet(params: {
        "sid"?: string;
    }, options?: any): Promise<Text>;
    /**
     * Прочитать пользовательское соглашение
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2TermsOfUseReadPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Пропустить пользовательское соглашение
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2TermsOfUseSkipPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Создать ТС
     * @param bodyUsefulHeight Полезная высота (м)
     * @param bodyUsefulLength Полезная длина (м)
     * @param bodyUsefulWidth Полезная ширина (м)
     * @param weight Грузоподъемность (кг)
     * @param sid Идентификатор сессии
     * @param employeeDriverId Идентификатор сотрудника (водителя)
     * @param vehicleBodyTypeId Идентификатор типа кузова ТС
     * @param ownerLegalPersonName Название собственника
     * @param ownerNaturalPersonFirstName Имя собственника
     * @param ownerNaturalPersonLastName Фамилия собственника
     * @param ownerNaturalPersonMiddleName Отчество собственника
     * @param ownerType Тип собственника
     * @param vehiclesPropsIds Идентификаторы характеристик ТС
     * @param vehicleTypeId Идентификатор типа ТС
     * @param volume Объем (куб. м)
     */
    dashboardRegistration2VehicleCreatePost(params: {
        "bodyUsefulHeight": number;
        "bodyUsefulLength": number;
        "bodyUsefulWidth": number;
        "weight": number;
        "sid"?: string;
        "employeeDriverId"?: number;
        "vehicleBodyTypeId"?: number;
        "ownerLegalPersonName"?: string;
        "ownerNaturalPersonFirstName"?: string;
        "ownerNaturalPersonLastName"?: string;
        "ownerNaturalPersonMiddleName"?: string;
        "ownerType"?: string;
        "vehiclesPropsIds"?: Array<number>;
        "vehicleTypeId"?: number;
        "volume"?: number;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Удалить ТС
     * @param id Идентификатор
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2VehicleDeletePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить список ТС
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2VehicleGetListGet(params: {
        "sid"?: string;
    }, options?: any): Promise<VehicleList>;
    /**
     * Следующее
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2VehicleNextPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Обновить ТС
     * @param id Идентификатор
     * @param bodyUsefulHeight Полезная высота (м)
     * @param bodyUsefulLength Полезная длина (м)
     * @param bodyUsefulWidth Полезная ширина (м)
     * @param weight Грузоподъемность (кг)
     * @param sid Идентификатор сессии
     * @param employeeDriverId Идентификатор сотрудника (водителя)
     * @param vehicleBodyTypeId Идентификатор типа кузова ТС
     * @param ownerLegalPersonName Название собственника
     * @param ownerNaturalPersonFirstName Имя собственника
     * @param ownerNaturalPersonLastName Фамилия собственника
     * @param ownerNaturalPersonMiddleName Отчество собственника
     * @param ownerType Тип собственника
     * @param vehiclesPropsIds Идентификаторы характеристик ТС
     * @param vehicleTypeId Идентификатор типа ТС
     * @param volume Объем (куб. м)
     */
    dashboardRegistration2VehicleUpdatePost(params: {
        "id": number;
        "bodyUsefulHeight": number;
        "bodyUsefulLength": number;
        "bodyUsefulWidth": number;
        "weight": number;
        "sid"?: string;
        "employeeDriverId"?: number;
        "vehicleBodyTypeId"?: number;
        "ownerLegalPersonName"?: string;
        "ownerNaturalPersonFirstName"?: string;
        "ownerNaturalPersonLastName"?: string;
        "ownerNaturalPersonMiddleName"?: string;
        "ownerType"?: string;
        "vehiclesPropsIds"?: Array<number>;
        "vehicleTypeId"?: number;
        "volume"?: number;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Загрузить файл
     * @param file Файл
     * @param type Тип файла
     * @param vehicleId Идентификатор ТС
     * @param sid Идентификатор сессии
     */
    dashboardRegistration2VehicleUploadFilePost(params: {
        "file": string;
        "type": string;
        "vehicleId": number;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Получить загруженные файлы
     * @param sid Идентификатор сессии
     */
    dashboardRegistrationGetUploadedFilesGet(params: {
        "sid"?: string;
    }, options?: any): Promise<UploadedFileListWithMeta>;
    /**
     * Получить типы загруженных файлов
     * @param sid Идентификатор сессии
     */
    dashboardRegistrationGetUploadedFilesTypesGet(params: {
        "sid"?: string;
    }, options?: any): Promise<UploadedFileTypeList>;
    /**
     * Удалить загруженный файл
     * @param id Идентификатор
     * @param sid Идентификатор сессии
     */
    dashboardRegistrationRemoveUploadedFilePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отправить данные регистрации
     * @param sid Идентификатор сессии
     * @param body Форма регистрации
     */
    dashboardRegistrationSubmitPost(params: {
        "sid"?: string;
        "body"?: Body;
    }, options?: any): Promise<Success>;
    /**
     * Загрузить файл
     * @param file Файл
     * @param type Тип файла
     * @param sid Идентификатор сессии
     */
    dashboardRegistrationUploadFilePost(params: {
        "file": string;
        "type": string;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Создать ТС
     * @param volume Объем (куб. м)
     * @param weight Вес (кг)
     * @param sid Идентификатор сессии
     * @param brand Марка
     * @param brandModel Модель
     * @param number Номер
     */
    dashboardVehicleCreatePost(params: {
        "volume": number;
        "weight": number;
        "sid"?: string;
        "brand"?: string;
        "brandModel"?: string;
        "number"?: string;
    }, options?: any): Promise<SuccessCreate>;
    /**
     * Получить ТС
     * @param id Идентификатор объекта
     * @param sid Идентификатор сессии
     */
    dashboardVehicleGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Vehicle>;
    /**
     * Получить список ТС
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    dashboardVehicleGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<VehicleListWithMeta>;
    /**
     * Удалить ТС
     * @param vehicleId Идентификатор ТС
     * @param sid Идентификатор сессии
     */
    dashboardVehicleRemovePost(params: {
        "vehicleId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Обновить ТС
     * @param vehicleId Идентификатор ТС
     * @param volume Объем (куб. м)
     * @param weight Вес (кг)
     * @param sid Идентификатор сессии
     * @param brand Марка
     * @param brandModel Модель
     * @param number Номер
     */
    dashboardVehicleUpdatePost(params: {
        "vehicleId": number;
        "volume": number;
        "weight": number;
        "sid"?: string;
        "brand"?: string;
        "brandModel"?: string;
        "number"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Подтвердить авторизацию
     * @param code Код подтверждения телефона
     * @param sid Идентификатор сессии
     */
    driverAuthorizationLoginConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Авторизовать
     * @param phone Телефон
     */
    driverAuthorizationLoginPost(params: {
        "phone": string;
    }, options?: any): Promise<SuccessSessionId>;
    /**
     * Деавторизовать
     * @param sid Идентификатор сессии
     */
    driverAuthorizationLogoutPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить список сообщений
     * @param cargoId Идентификатор груза
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoMessageGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoMessageListWithMeta>;
    /**
     * Отправить сообщение
     * @param cargoId Идентификатор груза
     * @param text Текст
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoMessageSubmitPost(params: {
        "cargoId": number;
        "text": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отменить предложение
     * @param cargoOfferId Идентификатор предложения
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoOfferCancelPost(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Подтвердить предложение
     * @param cargoOfferId Идентификатор предложения
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoOfferConfirmPost(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить предложение
     * @param cargoOfferId Идентификатор предложения
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoOfferGetGet(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): Promise<CargoOffer>;
    /**
     * Получить список предложений
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoOfferGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoOfferListWithMeta>;
    /**
     * Завершить перевозку груза
     * @param cargoRequestId Идентификатор отклика
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoRequestCompletePost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить отклик
     * @param cargoRequestId Идентификатор отклика
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoRequestGetGet(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequest>;
    /**
     * Получить список откликов
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoRequestGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequestListWithMeta>;
    /**
     * Начать перевозку груза
     * @param cargoRequestId Идентификатор отклика
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoRequestLoadPost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Отправиться за грузом
     * @param cargoRequestId Идентификатор отклика
     * @param sid Идентификатор сессии
     */
    driverDashboardCargoRequestWentPost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить список транзакций
     * @param limit Лимит
     * @param page Страница
     * @param sid Идентификатор сессии
     */
    driverDashboardPaymentTransactionGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<PaymentTransactionListWithMeta>;
    /**
     * Отправить заявку на списание средств
     * @param amount Сумма
     * @param sid Идентификатор сессии
     */
    driverDashboardPaymentTransactionSubmitRequestPost(params: {
        "amount": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Получить профиль
     * @param sid Идентификатор сессии
     */
    driverDashboardProfileGetGet(params: {
        "sid"?: string;
    }, options?: any): Promise<Driver>;
    /**
     * Обновить профиль
     * @param email E-mail
     * @param firstName Имя
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param phone Телефон
     * @param sid Идентификатор сессии
     */
    driverDashboardProfileUpdatePost(params: {
        "email": string;
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Проверить телефон
     * @param phone Телефон
     */
    userAuthorizationCheckPhonePost(params: {
        "phone": string;
    }, options?: any): Promise<Success>;
    /**
     * Авторизовать по e-mail
     * @param email E-mail
     * @param password Пароль
     */
    userAuthorizationLoginByEmailPost(params: {
        "email": string;
        "password": string;
    }, options?: any): Promise<SuccessSessionId>;
    /**
     * Подтвердить телефон
     * @param code Код подтверждения телефона
     * @param sid Идентификатор сессии
     */
    userAuthorizationLoginByPhoneConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Авторизовать по телефону
     * @param phone Телефон
     */
    userAuthorizationLoginByPhonePost(params: {
        "phone": string;
    }, options?: any): Promise<SuccessSessionId>;
    /**
     * Деавторизовать
     * @param sid Идентификатор сессии
     */
    userAuthorizationLogoutPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Подтвердить телефон
     * @param code Код подтверждения телефона
     * @param sid Идентификатор сессии
     */
    userRegistration2RegisterConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Зарегистрировать
     * @param accountSubtype Подтип аккаунта
     * @param accountType Тип аккаунта
     * @param firstName Имя
     * @param lastName Фамилия
     * @param middleName Отчество
     * @param phone Телефон
     * @param email E-mail
     */
    userRegistration2RegisterPost(params: {
        "accountSubtype": string;
        "accountType": string;
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "email"?: string;
    }, options?: any): Promise<SuccessSessionId>;
    /**
     * Подтвердить телефон
     * @param code Код подтверждения телефона
     * @param sid Идентификатор сессии
     */
    userRegistrationRegisterConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    /**
     * Зарегистрировать
     * @param body Регистрация
     */
    userRegistrationRegisterPost(params: {
        "body"?: Body1;
    }, options?: any): Promise<SuccessSessionId>;
}
/**
 * DefaultApi - factory interface
 */
export declare const DefaultApiFactory: (fetch?: FetchAPI, basePath?: string) => {
    autocompleteBankDataGet(params: {
        "bankBik": string;
    }, options?: any): Promise<AutocompleteBankDataList>;
    autocompleteCompanyDataGet(params: {
        "inn": string;
    }, options?: any): Promise<AutocompleteCompanyDataList>;
    autocompleteGet(params: {
        "modelAlias": string;
        "q": string;
    }, options?: any): Promise<AutocompleteList>;
    classifierGetBankDataGet(params: {
        "bankBik": string;
    }, options?: any): Promise<AutocompleteBankData>;
    classifierGetCompanyDataGet(params: {
        "inn": string;
    }, options?: any): Promise<AutocompleteCompanyData>;
    classifierGetListModelAliasGet(params: {
        "modelAlias": string;
    }, options?: any): Promise<ListItem[]>;
    classifierGetListVehicleTypeGet(options?: any): Promise<VehicleTypeList>;
    classifierGetListsModelsAliasesGet(params: {
        "modelsAliases": string;
    }, options?: any): Promise<Lists>;
    configGetListGet(options?: any): Promise<ConfigList>;
    dashboardCardBindPost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): Promise<SuccessUrl>;
    dashboardCardBindWithoutCardPost(params: {
        "sid"?: string;
    }, options?: any): Promise<SuccessForm>;
    dashboardCardCreatePost(params: {
        "cvv": number;
        "expireMonth": number;
        "expireYear": number;
        "number": number;
        "owner": string;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    dashboardCardGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Card>;
    dashboardCardGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CardListWithMeta>;
    dashboardCardRemovePost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCardSavePost(params: {
        "cvv": number;
        "expireMonth": number;
        "expireYear": number;
        "number": number;
        "owner": string;
        "sid"?: string;
    }, options?: any): Promise<SuccessUrl>;
    dashboardCardUnbindPost(params: {
        "cardId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoCalculateRecommendedPricePost(params: {
        "hours": number;
        "paymentType": string;
        "routeBeginning": string;
        "routeEnding": string;
        "sid"?: string;
        "vehicleTypeId"?: number;
    }, options?: any): Promise<CalculatedRecommendedPrice>;
    dashboardCargoCreatePost(params: {
        "dateBeginning": Date;
        "dateEnding": Date;
        "dimensionsHeight": number;
        "dimensionsLength": number;
        "dimensionsWidth": number;
        "paymentType": string;
        "routeBeginning": string;
        "routeEnding": string;
        "timeBeginning": string;
        "volume": number;
        "weight": number;
        "sid"?: string;
        "description"?: string;
        "name"?: string;
        "routeIntermediates"?: string[];
        "categoryId"?: number;
        "paymentCardId"?: number;
        "price"?: number;
    }, options?: any): Promise<SuccessCreate>;
    dashboardCargoDriverGetGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Driver>;
    dashboardCargoGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Cargo>;
    dashboardCargoGetListGet(params: {
        "mode"?: string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoListWithMeta>;
    dashboardCargoGetListWithRequestsGet(params: {
        "dataMode"?: string;
        "mode"?: string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoWithRequestsListWithMeta>;
    dashboardCargoGetResponsiblePersonsGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<CargoResponsiblePersonList>;
    dashboardCargoMessageGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoMessageListWithMeta>;
    dashboardCargoMessageSubmitPost(params: {
        "cargoId": number;
        "text": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestContractorAddPost(params: {
        "cargoId": number;
        "extraHourPrice": number;
        "minHours": number;
        "price": number;
        "vehicleId": number;
        "sid"?: string;
        "comment"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestContractorCancelPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestContractorCompletePost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestContractorConfirmPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestContractorLoadPost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestContractorWentPost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestCustomerAcceptPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestCustomerCancelPost(params: {
        "cargoId": number;
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestCustomerCompletePost(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardCargoRequestGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequest>;
    dashboardCargoRequestGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequestListWithMeta>;
    dashboardCargoRequestGetListGet_1(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequestListWithMeta>;
    dashboardCargoTrackGetLastGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoTrack>;
    dashboardCargoTrackGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoTrackListWithMeta>;
    dashboardCargoTrackSubmitPost(params: {
        "cargoId": number;
        "coordsLat": number;
        "coordsLng": number;
        "sid"?: string;
        "coordsAlt"?: number;
        "course"?: number;
        "speed"?: number;
    }, options?: any): Promise<Success>;
    dashboardCargoVehicleGetGet(params: {
        "cargoId": number;
        "sid"?: string;
    }, options?: any): Promise<Vehicle>;
    dashboardGeolocationGetLastGet(params: {
        "sid"?: string;
    }, options?: any): Promise<UserGeolocation>;
    dashboardGeolocationGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<UserGeolocationListWithMeta>;
    dashboardGeolocationSubmitPost(params: {
        "coordsLat": number;
        "coordsLng": number;
        "sid"?: string;
        "coordsAlt"?: number;
        "course"?: number;
        "speed"?: number;
    }, options?: any): Promise<Success>;
    dashboardPaymentTransactionGetBalanceGet(params: {
        "sid"?: string;
    }, options?: any): Promise<PaymentTransactionBalance>;
    dashboardPaymentTransactionGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<PaymentTransactionListWithMeta>;
    dashboardPaymentTransactionSubmitPayoutPost(params: {
        "amount": number;
        "cardNumber": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardPaymentTransactionSubmitRequestPost(params: {
        "amount": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardPersonGetGet(params: {
        "personId": number;
        "sid"?: string;
    }, options?: any): Promise<Person>;
    dashboardPersonGetListByIdsGet(params: {
        "personsIds": string;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<PersonList>;
    dashboardPersonGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<PersonListWithMeta>;
    dashboardProfileGetFullGet(params: {
        "sid"?: string;
    }, options?: any): Promise<FullProfile>;
    dashboardProfileGetGet(params: {
        "sid"?: string;
    }, options?: any): Promise<Profile>;
    dashboardProfileUpdatePost(params: {
        "email": string;
        "phone": string;
        "name": string;
        "sid"?: string;
        "briefAbout"?: string;
    }, options?: any): Promise<Success>;
    dashboardPushSubscribePost(params: {
        "userId": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardPushUnsubscribePost(params: {
        "userId": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2DataSaveBusinessmanPost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "bankBik"?: number;
        "bankKs"?: number;
        "bankName"?: string;
        "inn"?: number;
        "legalAddressFull"?: string;
        "legalAddressOffice"?: string;
        "legalAddressPostOfficeBox"?: string;
        "legalAddressPostalIndex"?: number;
        "ogrn"?: number;
        "rs"?: number;
        "taxationTypeId"?: number;
        "town"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2DataSaveLegalPersonPost(params: {
        "name": string;
        "sid"?: string;
        "bankBik"?: number;
        "bankKs"?: number;
        "bankName"?: string;
        "flagWithVat"?: number;
        "inn"?: number;
        "kpp"?: number;
        "legalAddressFull"?: string;
        "legalAddressOffice"?: string;
        "legalAddressPostOfficeBox"?: string;
        "legalAddressPostalIndex"?: number;
        "ogrn"?: number;
        "rs"?: number;
        "taxationTypeId"?: number;
        "town"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2DataSavePhysicalPersonPost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "town"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2DataUploadFilePost(params: {
        "file": string;
        "type": string;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    dashboardRegistration2EmployeeConfirmPost(params: {
        "id": number;
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2EmployeeCreatePost(params: {
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "sid"?: string;
        "email"?: string;
        "roles"?: string[];
    }, options?: any): Promise<SuccessSpecial1>;
    dashboardRegistration2EmployeeDeletePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2EmployeeGetListGet(params: {
        "sid"?: string;
    }, options?: any): Promise<UserList>;
    dashboardRegistration2EmployeeNextPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2EmployeeUpdatePost(params: {
        "firstName": string;
        "id": number;
        "lastName": string;
        "middleName": string;
        "sid"?: string;
        "roles"?: string[];
    }, options?: any): Promise<Success>;
    dashboardRegistration2EmployeeUploadFilePost(params: {
        "file": string;
        "type": string;
        "userId": number;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    dashboardRegistration2GetStatusGet(params: {
        "sid"?: string;
    }, options?: any): Promise<PersonRegistrationStatus>;
    dashboardRegistration2TermsOfUseGetTextGet(params: {
        "sid"?: string;
    }, options?: any): Promise<Text>;
    dashboardRegistration2TermsOfUseReadPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2TermsOfUseSkipPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2VehicleCreatePost(params: {
        "bodyUsefulHeight": number;
        "bodyUsefulLength": number;
        "bodyUsefulWidth": number;
        "weight": number;
        "sid"?: string;
        "employeeDriverId"?: number;
        "vehicleBodyTypeId"?: number;
        "ownerLegalPersonName"?: string;
        "ownerNaturalPersonFirstName"?: string;
        "ownerNaturalPersonLastName"?: string;
        "ownerNaturalPersonMiddleName"?: string;
        "ownerType"?: string;
        "vehiclesPropsIds"?: number[];
        "vehicleTypeId"?: number;
        "volume"?: number;
    }, options?: any): Promise<SuccessCreate>;
    dashboardRegistration2VehicleDeletePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2VehicleGetListGet(params: {
        "sid"?: string;
    }, options?: any): Promise<VehicleList>;
    dashboardRegistration2VehicleNextPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistration2VehicleUpdatePost(params: {
        "id": number;
        "bodyUsefulHeight": number;
        "bodyUsefulLength": number;
        "bodyUsefulWidth": number;
        "weight": number;
        "sid"?: string;
        "employeeDriverId"?: number;
        "vehicleBodyTypeId"?: number;
        "ownerLegalPersonName"?: string;
        "ownerNaturalPersonFirstName"?: string;
        "ownerNaturalPersonLastName"?: string;
        "ownerNaturalPersonMiddleName"?: string;
        "ownerType"?: string;
        "vehiclesPropsIds"?: number[];
        "vehicleTypeId"?: number;
        "volume"?: number;
    }, options?: any): Promise<SuccessCreate>;
    dashboardRegistration2VehicleUploadFilePost(params: {
        "file": string;
        "type": string;
        "vehicleId": number;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    dashboardRegistrationGetUploadedFilesGet(params: {
        "sid"?: string;
    }, options?: any): Promise<UploadedFileListWithMeta>;
    dashboardRegistrationGetUploadedFilesTypesGet(params: {
        "sid"?: string;
    }, options?: any): Promise<UploadedFileTypeList>;
    dashboardRegistrationRemoveUploadedFilePost(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardRegistrationSubmitPost(params: {
        "sid"?: string;
        "body"?: Body;
    }, options?: any): Promise<Success>;
    dashboardRegistrationUploadFilePost(params: {
        "file": string;
        "type": string;
        "sid"?: string;
    }, options?: any): Promise<SuccessCreate>;
    dashboardVehicleCreatePost(params: {
        "volume": number;
        "weight": number;
        "sid"?: string;
        "brand"?: string;
        "brandModel"?: string;
        "number"?: string;
    }, options?: any): Promise<SuccessCreate>;
    dashboardVehicleGetIdGet(params: {
        "id": number;
        "sid"?: string;
    }, options?: any): Promise<Vehicle>;
    dashboardVehicleGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<VehicleListWithMeta>;
    dashboardVehicleRemovePost(params: {
        "vehicleId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    dashboardVehicleUpdatePost(params: {
        "vehicleId": number;
        "volume": number;
        "weight": number;
        "sid"?: string;
        "brand"?: string;
        "brandModel"?: string;
        "number"?: string;
    }, options?: any): Promise<Success>;
    driverAuthorizationLoginConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverAuthorizationLoginPost(params: {
        "phone": string;
    }, options?: any): Promise<SuccessSessionId>;
    driverAuthorizationLogoutPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverDashboardCargoMessageGetListGet(params: {
        "cargoId": number;
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoMessageListWithMeta>;
    driverDashboardCargoMessageSubmitPost(params: {
        "cargoId": number;
        "text": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverDashboardCargoOfferCancelPost(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverDashboardCargoOfferConfirmPost(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverDashboardCargoOfferGetGet(params: {
        "cargoOfferId": number;
        "sid"?: string;
    }, options?: any): Promise<CargoOffer>;
    driverDashboardCargoOfferGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoOfferListWithMeta>;
    driverDashboardCargoRequestCompletePost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverDashboardCargoRequestGetGet(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequest>;
    driverDashboardCargoRequestGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<CargoRequestListWithMeta>;
    driverDashboardCargoRequestLoadPost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverDashboardCargoRequestWentPost(params: {
        "cargoRequestId": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverDashboardPaymentTransactionGetListGet(params: {
        "limit"?: number;
        "page"?: number;
        "sid"?: string;
    }, options?: any): Promise<PaymentTransactionListWithMeta>;
    driverDashboardPaymentTransactionSubmitRequestPost(params: {
        "amount": number;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    driverDashboardProfileGetGet(params: {
        "sid"?: string;
    }, options?: any): Promise<Driver>;
    driverDashboardProfileUpdatePost(params: {
        "email": string;
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    userAuthorizationCheckPhonePost(params: {
        "phone": string;
    }, options?: any): Promise<Success>;
    userAuthorizationLoginByEmailPost(params: {
        "email": string;
        "password": string;
    }, options?: any): Promise<SuccessSessionId>;
    userAuthorizationLoginByPhoneConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    userAuthorizationLoginByPhonePost(params: {
        "phone": string;
    }, options?: any): Promise<SuccessSessionId>;
    userAuthorizationLogoutPost(params: {
        "sid"?: string;
    }, options?: any): Promise<Success>;
    userRegistration2RegisterConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    userRegistration2RegisterPost(params: {
        "accountSubtype": string;
        "accountType": string;
        "firstName": string;
        "lastName": string;
        "middleName": string;
        "phone": string;
        "email"?: string;
    }, options?: any): Promise<SuccessSessionId>;
    userRegistrationRegisterConfirmPost(params: {
        "code": string;
        "sid"?: string;
    }, options?: any): Promise<Success>;
    userRegistrationRegisterPost(params: {
        "body"?: Body1;
    }, options?: any): Promise<SuccessSessionId>;
};
